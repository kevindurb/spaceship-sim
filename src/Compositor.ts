import { VideoBuffer } from './VideoBuffer.js';

type DrawFunction = (buffer: VideoBuffer) => void;

export class Compositor {
  public layers: DrawFunction[] = [];

  draw(videoBuffer: VideoBuffer) {
    for (const layer of this.layers) {
      videoBuffer.context.save();
      layer(videoBuffer);
      videoBuffer.context.restore();
    }
  }
}
