import { VideoBuffer } from './VideoBuffer.js';

export class GameContext {
  constructor(
    public videoBuffer: VideoBuffer,
    public deltaTime = 0,
  ) {}
}
