import { Vector2d } from './math/Vector2d.js';
import { VideoBuffer } from './VideoBuffer.js';

export class GameScreen {
  public videoBuffer = new VideoBuffer(
    new Vector2d(window.innerWidth, window.innerHeight),
  );

  constructor() {
    document.body.append(this.videoBuffer.canvas);
  }
}
