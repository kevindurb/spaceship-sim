export class Vector2d {
  constructor(
    public x = 0,
    public y = 0,
  ) {}

  add(v: Vector2d) {
    this.x += v.x;
    this.y += v.y;
    return this;
  }

  multiply(v: number) {
    this.x *= v;
    this.y *= v;
    return this;
  }

  copy() {
    return new Vector2d(this.x, this.y);
  }
}
