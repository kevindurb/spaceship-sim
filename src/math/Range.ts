export class Range {
  constructor(
    private min: number,
    private max: number,
  ) {}

  forEach(callback: (value: number) => void) {
    for (let value = this.min; value <= this.max; value++) {
      callback(value);
    }
  }
}
