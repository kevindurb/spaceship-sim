import type { Entity } from './Entity.js';
import type { GameContext } from './GameContext.js';
import { Compositor } from './Compositor.js';
import type { VideoBuffer } from './VideoBuffer.js';

export abstract class Screen {
  compositor = new Compositor();
  entities = new Map<typeof Entity, Entity>();

  update(context: GameContext) {
    for (const [, entity] of this.entities.entries()) {
      entity.update(context);
    }
  }

  draw(videoBuffer: VideoBuffer) {
    this.compositor.draw(videoBuffer);
  }
}
