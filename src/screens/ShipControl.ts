import { Ship } from '../entities/Ship.js';
import { KeyboardInput } from '../KeyboardInput.js';
import { buildBackground } from '../layers/Background.js';
import { buildSpriteLayer } from '../layers/Sprites.js';
import { Screen } from '../Screen.js';

export class ShipControl extends Screen {
  private keyboardInput = new KeyboardInput();

  constructor() {
    super();
    const ship = new Ship();
    this.entities.set(Ship, ship);

    this.compositor.layers.push(buildBackground());
    this.compositor.layers.push(buildSpriteLayer(ship));

    this.keyboardInput.addMapping('KeyW', (state) => {
      ship.thrust.acceleration.y = state === 'Pressed' ? 10 : 0;
    });

    this.keyboardInput.addMapping('KeyS', (state) => {
      ship.thrust.acceleration.y = state === 'Pressed' ? -10 : 0;
    });

    this.keyboardInput.addMapping('KeyA', (state) => {
      ship.rotate.rotationDirection = state === 'Pressed' ? 1 : 0;
    });

    this.keyboardInput.addMapping('KeyD', (state) => {
      ship.rotate.rotationDirection = state === 'Pressed' ? -1 : 0;
    });

    this.keyboardInput.listenTo(window);
  }
}
