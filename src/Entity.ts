import type { GameContext } from './GameContext.js';
import type { Trait } from './Trait.js';
import { Vector2d } from './math/Vector2d.js';
import { VideoBuffer } from './VideoBuffer.js';

export abstract class Entity {
  public location = new Vector2d();
  public velocity = new Vector2d();
  public rotation = 0;
  public abstract size: Vector2d;

  public update(context: GameContext) {
    for (const trait of this.traits) {
      trait.update(this, context);
    }
  }

  protected abstract get traits(): Trait[];

  public abstract draw(buffer: VideoBuffer): void;
}
