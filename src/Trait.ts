import type { GameContext } from './GameContext.js';
import type { Entity } from './Entity.js';

export abstract class Trait {
  public abstract update(entity: Entity, context: GameContext): void;
}
