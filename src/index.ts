import { Timer } from './Timer.js';
import { GameContext } from './GameContext.js';
import { GameScreen } from './GameScreen.js';
import { ShipControl } from './screens/ShipControl.js';

(async function main() {
  const gameScreen = new GameScreen();
  const context = new GameContext(gameScreen.videoBuffer);
  const shipControl = new ShipControl();

  const update = (deltaTime: number) => {
    context.deltaTime = deltaTime;
    shipControl.update(context);
    context.videoBuffer.clear();
    shipControl.draw(context.videoBuffer);
  };

  const timer = new Timer(1 / 60, update);

  timer.start();
})();
