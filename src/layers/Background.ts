import { Range } from '../math/Range.js';
import { Vector2d } from '../math/Vector2d.js';
import { VideoBuffer } from '../VideoBuffer.js';

export function buildBackground() {
  const size = new Vector2d(window.innerWidth, window.innerHeight);
  const videoBuffer = new VideoBuffer(size);

  new Range(0, size.x / 100).forEach((value) => {
    videoBuffer.context.fillRect(value * 100, 0, 1, size.y);
  });

  new Range(0, size.y / 100).forEach((value) => {
    videoBuffer.context.fillRect(0, value * 100, size.x, 1);
  });

  return function drawBackground(buffer: VideoBuffer) {
    videoBuffer.drawTo(buffer, new Vector2d());
  };
}
