import { Entity } from '../Entity.js';
import { VideoBuffer } from '../VideoBuffer.js';

export function buildSpriteLayer(entity: Entity) {
  return function drawSprites(buffer: VideoBuffer) {
    entity.draw(buffer);
  };
}
