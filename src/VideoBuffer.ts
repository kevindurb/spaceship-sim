import { Vector2d } from './math/Vector2d.js';

export class VideoBuffer {
  public canvas = document.createElement('canvas');

  constructor(public size: Vector2d) {
    this.canvas.width = size.x;
    this.canvas.height = size.y;
  }

  public get context() {
    const context = this.canvas.getContext('2d');
    if (!context) throw new Error('Error getting canvas context');
    return context;
  }

  public clear() {
    this.context.clearRect(0, 0, this.size.x, this.size.y);
  }

  public drawTo(videoBuffer: VideoBuffer, position: Vector2d) {
    videoBuffer.context.drawImage(this.canvas, position.x, position.y);
  }

  public draw(videoBuffer: VideoBuffer, position: Vector2d) {
    this.context.drawImage(videoBuffer.canvas, position.x, position.y);
  }
}
