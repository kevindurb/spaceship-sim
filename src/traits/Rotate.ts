import type { Entity } from '../Entity.js';
import { Trait } from '../Trait.js';
import type { GameContext } from '../GameContext.js';

export class Rotate extends Trait {
  public rotationDirection = 0;
  public rotationVelocity = 100; // deg/sec

  public update(entity: Entity, { deltaTime }: GameContext) {
    const rotationDelta =
      this.rotationDirection * (this.rotationVelocity * deltaTime);
    entity.rotation += rotationDelta;

    if (entity.rotation > 360) entity.rotation -= 360;

    if (entity.rotation < 0) entity.rotation += 360;
  }
}
