import type { Entity } from '../Entity.js';
import { Trait } from '../Trait.js';
import type { GameContext } from '../GameContext.js';

export class Physics extends Trait {
  public update(entity: Entity, { deltaTime }: GameContext) {
    entity.location.add(entity.velocity.copy().multiply(deltaTime));
  }
}
