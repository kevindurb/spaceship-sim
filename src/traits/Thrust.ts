import type { Entity } from '../Entity.js';
import { Trait } from '../Trait.js';
import type { GameContext } from '../GameContext.js';
import { Vector2d } from '../math/Vector2d.js';

function toRad(deg: number) {
  return deg * (Math.PI / 180);
}

export class Thrust extends Trait {
  public acceleration = new Vector2d();

  public update(entity: Entity, { deltaTime }: GameContext) {
    const { y: accel } = this.acceleration;
    const accelX = Math.sin(toRad(entity.rotation)) * accel;
    const accelY = Math.cos(toRad(entity.rotation)) * accel;
    const rotatedAccel = new Vector2d(accelX, accelY);

    entity.velocity.add(rotatedAccel.multiply(deltaTime));
  }
}
