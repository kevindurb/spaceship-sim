import { Entity } from '../Entity.js';
import { Physics } from '../traits/Physics.js';
import { Vector2d } from '../math/Vector2d.js';
import { VideoBuffer } from '../VideoBuffer.js';
import { Thrust } from '../traits/Thrust.js';
import { Rotate } from '../traits/Rotate.js';

function toRad(deg: number) {
  return deg * (Math.PI / 180);
}

export class Ship extends Entity {
  public override size = new Vector2d(48, 64);
  public override location = new Vector2d(0, 0);
  public physics = new Physics();
  public thrust = new Thrust();
  public rotate = new Rotate();

  protected get traits() {
    return [this.physics, this.thrust, this.rotate];
  }

  public draw({ context }: VideoBuffer) {
    const { x: width, y: height } = this.size;
    const { x, y } = this.location;

    context.fillStyle = 'red';

    context.translate(x, y);
    context.rotate(toRad(-this.rotation));
    context.fillRect(width / -2, height / -2, width, height);
  }
}
